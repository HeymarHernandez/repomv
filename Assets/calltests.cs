﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using SimpleJSON;

public class calltests : MonoBehaviour
{

    public Text storename;
    public Text description;
    public Text adress;
    public Text phone;
    public Image companylogoimage;

    string url = "http://134.122.35.104";

    public GameObject productUI;

    public static string storeid= "600232bc4ff72f29dd565a1c";

    void OnEnable()
    {
        StartCoroutine(LoadByStoreID(storeid));
        StartCoroutine(ProductsByStoreID(storeid));
    }

    void Start()
    {
        //StartCoroutine(brands());
        //StartCoroutine(products());
    }

    void LateUpdate()
    {
        transform.Translate(0, Time.deltaTime, 0);
    }
    void Update()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        if (EventSystem.current.currentSelectedGameObject)
        {
            //print(EventSystem.current.currentSelectedGameObject.name);
            if (Input.GetMouseButton(0))
            {
                if (EventSystem.current.currentSelectedGameObject.transform.parent.name == "productcontainer")
                {
                    productUI.SetActive(true);
                    StartCoroutine(GetProductByID(EventSystem.current.currentSelectedGameObject));
                }
            }
        }

    }

    public GameObject brandprefab;
    public GameObject productprefab;
    public GameObject fullproductpopup;

    public Transform brandt;
    public Transform productt;


    IEnumerator gettexture(Image logoimage, string url)
    {
        UnityWebRequest llamada = UnityWebRequestTexture.GetTexture((url));
        yield return llamada.SendWebRequest();

        if (llamada.isNetworkError || llamada.isHttpError)
        {
            print(llamada.error);
        }
        else
        {
            Texture2D tex = DownloadHandlerTexture.GetContent(llamada);
            Sprite mysprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
            logoimage.sprite = mysprite;
        }
    }

    IEnumerator LoadByStoreID(string storeid)
    {
        UnityWebRequest www = UnityWebRequest.Get(url+":8000/api/brands");
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log(www.error);
        }
        else
        {
            var N = JSONNode.Parse(www.downloadHandler.text);
            for (int i = 0; i < N.Count; i++)
            {
                //print(N[i]["_id"].Value);
                if (N[i]["_id"].Value == storeid)
                {
                    storename.text = N[i]["brandName"].Value;
                    description.text = N[i]["description"].Value;
                    adress.text = N[i]["address"].Value;
                    phone.text = N[i]["phone"].Value;
                    StartCoroutine(gettexture(companylogoimage, N[i]["logo"][0]["url"].Value));
                }
            }

        }
    } 
    
    IEnumerator ProductsByStoreID(string storeid)
    {
        UnityWebRequest www = UnityWebRequest.Get(url + ":8000/api/products/0");
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log(www.error);
        }
        else
        {
            for (int i = 0; i < productt.transform.childCount; i++)
            {Destroy(productt.transform.GetChild(i).gameObject);}

            var N = JSONNode.Parse(www.downloadHandler.text);
            int itemscounter=0;

            for (int i = 0; i < N.Count; i++)
            {
                if (N[i]["brandId"].Value == storeid)
                {
                    itemscounter++;
                    GameObject clone = GameObject.Instantiate(productprefab);
                    clone.transform.SetParent(productt);
                    clone.transform.localScale = Vector3.one;
                    clone.name = N[i]["_id"].Value;

                    StartCoroutine(gettexture(clone.transform.GetChild(0).GetComponent<Image>(), N[i]["images"][0]["url"].Value));
                    clone.transform.GetChild(1).GetComponent<Text>().text = N[i]["price"].Value;
                    clone.transform.GetChild(2).GetComponent<Text>().text = N[i]["title"].Value;
                    GameObject starscontainer = clone.transform.GetChild(3).gameObject;
                    
                    for (int j = 0; j < starscontainer.transform.childCount; j++)
                    {
                        starscontainer.transform.GetChild(j).GetChild(0).gameObject.SetActive(false);
                        if (N[i]["ratings"].Value.Length > 0)
                        {
                            if (int.Parse(N[i]["ratings"].Value) > j)
                            {starscontainer.transform.GetChild(j).GetChild(0).gameObject.SetActive(true);}
                        }
                    }
                }
            }
            productt.transform.localPosition = new Vector3(0,-300,0);
            productt.GetComponent<RectTransform>().sizeDelta = new Vector2(550, 167 * (1+((int)itemscounter / 5)));
        }
    }

    //piwkeyeyu

    public Text productname;
    public Text itmdescription;
    public Text itmprice;
    public GameObject itemrating;
    public Image itmimage;



    IEnumerator GetProductByID(GameObject targetobject)
    {
        string ProductID = targetobject.name;
        UnityWebRequest www = UnityWebRequest.Get(url + ":8000/api/products/0");
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log(www.error);
        }
        else
        {
            var N = JSONNode.Parse(www.downloadHandler.text);
            for (int i = 0; i < N.Count; i++)
            {
                if (N[i]["_id"].Value == ProductID)
                {
                    productname.text = N[i]["title"].Value;
                    itmdescription.text = N[i]["description"].Value;
                    itmprice.text = N[i]["price"].Value;
                    StartCoroutine(gettexture(itmimage, N[i]["images"][0]["url"].Value));


                    for (int j = 0; j < itemrating.transform.childCount; j++)
                    {
                        itemrating.transform.GetChild(j).GetChild(0).gameObject.SetActive(false);
                        if (N[i]["ratings"].Value.Length > 0)
                        {
                            if (int.Parse(N[i]["ratings"].Value) > j)
                            {itemrating.transform.GetChild(j).GetChild(0).gameObject.SetActive(true);}
                        }
                    }

                    //fullproductpopup.transform.GetChild(0)
                }

            }
        }
    }




    IEnumerator brands()
    {
        string get_url = url + ":8000/api/brands";
        WWW hs_get = new WWW(get_url);

        yield return hs_get;
        if (hs_get.error != null)
        {
            print("Hubo un error obteniendo los datos: " + hs_get.error);
        }
        else
        {
            var N = JSONNode.Parse(hs_get.text);
            for (int i = 0; i < N.Count; i++)
            {
                GameObject clone = GameObject.Instantiate(brandprefab);
                clone.transform.SetParent(brandt);
                clone.transform.localScale = Vector3.one;

                clone.transform.GetChild(0).GetComponent<Text>().text = N[i]["brandName"].Value;
                clone.transform.GetChild(1).GetComponent<Text>().text = N[i]["description"].Value;
                clone.transform.GetChild(2).GetComponent<Text>().text = N[i]["address"].Value;
                clone.transform.GetChild(3).GetComponent<Text>().text = N[i]["phone"].Value;
                StartCoroutine(gettexture(clone.transform.GetChild(4).GetComponent<Image>(), N[i]["logo"][0]["url"].Value));
                //print(N[i]["categories"].Value);
                //print(N[i]["subs"].Value);
                //print(N[i]["logo"][0]["public_id"].Value);
                //print(N[i]["logo"][0]["url"].Value);// logo url
                //print(N[i]["products"][0]["ref"].Value);
                //print(N[i]["role"].Value);
                print(N[i]["_id"].Value);
                //print(N[i]["repName"].Value);
                //print(N[i]["idUser"].Value);
                //print(N[i]["brandName"].Value);
                //print(N[i]["email"].Value);
                //print(N[i]["repId"].Value);
                //print(N[i]["phone"].Value);
                //print(N[i]["description"].Value);
                //print(N[i]["address"].Value);
                //print(N[i]["token"].Value);
                //print(N[i]["createdAt"].Value);
                //print(N[i]["updatedAt"].Value);
                //print(N[i]["__v"].Value);
            }
        }
    }
    IEnumerator products()
    {
        string get_url = url + ":8000/api/products/0";
        WWW hs_get = new WWW(get_url);

        yield return hs_get;
        if (hs_get.error != null)
        {
            print("Hubo un error obteniendo los datos: " + hs_get.error);
        }
        else
        {
            var N = JSONNode.Parse(hs_get.text);
            for (int i = 0; i < N.Count; i++)
            {
                GameObject clone = GameObject.Instantiate(productprefab);
                clone.transform.SetParent(productt);
                clone.transform.localScale = Vector3.one;

                clone.transform.GetChild(0).GetComponent<Text>().text = N[i]["title"].Value;
                clone.transform.GetChild(1).GetComponent<Text>().text = N[i]["description"].Value;
                clone.transform.GetChild(2).GetComponent<Text>().text = N[i]["price"].Value;
                StartCoroutine(gettexture(clone.transform.GetChild(3).GetComponent<Image>(), N[i]["images"][0]["url"].Value));

                //print(N[i]["subs"].Value);
                //print(N[i]["sold"].Value);
                //print(N[i]["images"][0]["public_id"].Value);
                //print(N[i]["images"][0]["url"].Value);
                //print(N[i]["_id"].Value);
                //print(N[i]["title"][0]["ref"].Value);
                //print(N[i]["description"].Value);
                //print(N[i]["price"].Value);
                //print(N[i]["shipping"].Value);
                //print(N[i]["quantity"].Value);
                //print(N[i]["category"]["id"].Value);
                //print(N[i]["category"]["name"].Value);
                //print(N[i]["category"]["slug"].Value);
                //print(N[i]["color"].Value);
                //print(N[i]["size"].Value);
                //print(N[i]["slug"].Value);
                //print(N[i]["brandId"].Value);
                //print(N[i]["ratings"].Value);
                //print(N[i]["createdAt"].Value);
                //print(N[i]["updatedAt"].Value);
                //print(N[i]["__v"].Value);


            }
        }
    }

}
