﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;

public class GameController : MonoBehaviour
{

    public static GameController instancia;

    public int idGame = 45;
    public int idUser;

    public float maxpersonalrecord;
    public float maximoglobal;

    public float GameTrofeo;
    public ArrayList ranking = new ArrayList();
    string PonitsUser, PointMax, User_Id;
    string token = "ADSHTJD5521S11D";
    public string URLServer = "http://54.69.38.89";
    int posrankpintar;
    public string Version;

    public static int mastermenuindex;

    public static int maximfordeath;
    public GameObject[] menugameloginscreen;
    public GameObject[] versiongame;
    public GameObject[] namesranking;
    public GameObject[] puntajeranking;

    public GameObject usuariotextfield;
    public GameObject contraseñatextfield;
    public GameObject botonaeliminar;

    public GameObject textobienvenida;
    public GameObject bannerbienvenida;
    public GameObject[] blackbars;

    public GameObject waiting;
    public GameObject userelements;

    GameObject singletonsalvaje;
    string tipodeurl;

    void Awake()
    {

#if UNITY_IOS
        ScriptSingleton.showupbaner=true;
        ScriptSingleton.keytutorial=false;
#endif

#if UNITY_ANDROID
        ScriptSingleton.showupbaner=true;
        ScriptSingleton.keytutorial=false;
#endif

#if UNITY_WEBPLAYER
        ScriptSingleton.showupbaner=false;
        ScriptSingleton.keytutorial=true;
#endif

#if UNITY_WEBGL
        ScriptSingleton.showupbaner = false;
        ScriptSingleton.keytutorial = true;
#endif

#if UNITY_STANDALONE
        ScriptSingleton.showupbaner=true;
        ScriptSingleton.keytutorial=true;
#endif

#if UNITY_EDITOR
        ScriptSingleton.showupbaner = false;
        ScriptSingleton.keytutorial = true;
#endif

        ScriptSingleton.idgame = idGame;
        ScriptSingleton.masterkey = token;
        if (ScriptSingleton.showupbaner)//si muestro el banner es porque no estoy alojado en web
        {
            tipodeurl = "https://kidon.co";
            ScriptSingleton.pruebas = false;

        }
        else
        {

            tipodeurl = Application.absoluteURL;

#if UNITY_EDITOR
            ScriptSingleton.showupbaner = true;
            tipodeurl = "http://54.69.38.89";
            ScriptSingleton.pruebas = true;
#endif

            if (tipodeurl.Substring(0, "https://www".Length).Equals("https://www"))
            {
                tipodeurl = "https://www.kidon.co";
                ScriptSingleton.pruebas = false;
            }
            else if (tipodeurl.Substring(0, "http://54.69.38.89".Length).Equals("http://54.69.38.89"))
            {
                tipodeurl = "http://54.69.38.89";
                ScriptSingleton.pruebas = true;
            }
            else
            {
                tipodeurl = "https://kidon.co";
                ScriptSingleton.pruebas = false;
            }
        }
        //      print(tipodeurl);

        ScriptSingleton.masterurl = tipodeurl;
        URLServer = tipodeurl;

        if (ScriptSingleton.iduser != 0)
        {
            idUser = ScriptSingleton.iduser;
            textobienvenida.GetComponent<Text>().text = "hola " + ScriptSingleton.playername;
        }
        getPoints_click();
        ScriptSingleton.iduser = idUser;
        StartCoroutine(GetRanking(idGame));
        for (int i = 0; i < versiongame.Length; i++)
        { versiongame[i].GetComponent<Text>().text = "Todos los derechos reservados kidon SAS © 2015. Versión " + Version + " - " + URLServer; }
        mastermenu(1);
    }

    public void mastermenu(int opcion)
    {
        if (idUser != 0)
        {
            if (ScriptSingleton.showupbaner)
            {
                botonaeliminar.SetActive(false);
                bannerbienvenida.SetActive(true);
            }
            else
            {
                botonaeliminar.SetActive(false);
                bannerbienvenida.SetActive(false);
            }
        }
        else
        {
            if (ScriptSingleton.showupbaner)
            {
                botonaeliminar.SetActive(true);
                bannerbienvenida.SetActive(false);
            }
            else
            {
                botonaeliminar.SetActive(false);
                bannerbienvenida.SetActive(false);
            }
        }
        if (opcion == 1)
        {
            print("what is this");

        }
        else if (opcion == 2)
        {
            ScriptSingleton.Puntaje = 0;
            ScriptSingleton.lives = 5;
            ScriptSingleton.level = 0;
            SceneManager.LoadScene(1);
        }
    }

    public void cerrarsecion()
    {
        idUser = 0;
        ScriptSingleton.iduser = 0;
        maxpersonalrecord = 0;
        mastermenu(1);
    }
    public void IraRegistro()
    {
        Application.OpenURL("https://www.kidon.co/login/#/elegirAvatar");
    }

    public void GetUser(string user_id)
    {
        idUser = int.Parse(user_id);
        ScriptSingleton.iduser = idUser;
        StartCoroutine(GetPoints(idUser, idGame));
    }
    public void getPoints_click()
    {
        StartCoroutine(GetPoints(idUser, idGame));
    }
    IEnumerator GetPoints(int id_user, int id_game)
    {
        string getPointsURL = URLServer + "/peticiones_game/solicutud_user";
        string get_url = getPointsURL + '/' + token + '/' + id_user + '/' + id_game;
        WWW hs_get = new WWW(get_url);

        print(get_url);
        yield return hs_get;
        if (hs_get.error != null)
        {
            print("Hubo un error obteniendo los datos: " + hs_get.error);
        }
        else
        {

            var N = JSONNode.Parse(hs_get.text);
            /////////////////////////////////////////posible error.

            if (ScriptSingleton.pruebas)
            {
                print("user exist" + hs_get.text);
            }

            if (idUser != 0)
            {
                if ((N["user"][0]["gender"].Value) != "")
                {
                    print((N["user"][0]["gender"].Value));
                    if (float.Parse(N["user"][0]["gender"].Value) == 2)
                    { ScriptSingleton.female = true; }
                    if (float.Parse(N["user"][0]["gender"].Value) == 1)
                    { ScriptSingleton.female = false; }
                }
            }

            if ((N["ranking_mayor"].Value) != "")
            {
                maximoglobal = float.Parse(N["ranking_mayor"].Value);
                maxpersonalrecord = float.Parse(N["puntos_user"].Value);
                ScriptSingleton.maximouser = maxpersonalrecord;
                ScriptSingleton.maximoglobal = maximoglobal;
            }
            else
            {
                maximoglobal = 0;
                maxpersonalrecord = 0;
                ScriptSingleton.maximouser = maxpersonalrecord;
                ScriptSingleton.maximoglobal = maximoglobal;
            }
        }
    }
    //obtenga de los texfield declarados globales el user y la contra.
    public void IngresarBtn()
    {
        usuariotextfield.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
        contraseñatextfield.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);

        if (usuariotextfield.GetComponent<InputField>().text != "" && contraseñatextfield.GetComponent<InputField>().text != "")
        {
            StartCoroutine(CheckLogin(usuariotextfield.GetComponent<InputField>().text, contraseñatextfield.GetComponent<InputField>().text));
        }
        else
        {
            if (usuariotextfield.GetComponent<InputField>().text == "")
            {
                usuariotextfield.GetComponent<Image>().color = new Color(1f, 0.8f, 0.8f, 1f);
            }
            if (contraseñatextfield.GetComponent<InputField>().text == "")
            {
                contraseñatextfield.GetComponent<Image>().color = new Color(1f, 0.8f, 0.8f, 1f);
            }
        }
    }


    IEnumerator CheckLogin(string User, string Pasword)
    {

        menugameloginscreen[1].transform.GetChild(2).gameObject.SetActive(true);
        string get_url = URLServer + "/peticiones_angular/login" + '/' + User + '/' + Pasword;
        WWW hs_get = new WWW(get_url);
        yield return hs_get;
        menugameloginscreen[1].transform.GetChild(2).gameObject.SetActive(false);
        print(get_url);
        if (hs_get.error != null)
        {
        }
        else
        {
            var N = JSONNode.Parse(hs_get.text);
            if (N["error"].Value == "true")
            {
                usuariotextfield.GetComponent<Image>().color = new Color(1f, 0.8f, 0.8f, 1f);
                contraseñatextfield.GetComponent<Image>().color = new Color(1f, 0.8f, 0.8f, 1f);
                contraseñatextfield.GetComponent<InputField>().text = "";
            }
            else
            {
                idUser = int.Parse(N["id_user"].Value);
                string get_url2 = URLServer + "/peticiones_game/solicutud_user" + '/' + token + '/' + idUser + '/' + idGame;

                WWW hs_get2 = new WWW(get_url2);
                yield return hs_get2;
                if (hs_get2.error != null)
                {
                }
                else
                {
                    var N2 = JSONNode.Parse(hs_get2.text);

                    if (idUser != 0)
                    {
                        if ((N2["user"][0]["gender"].Value) != "")
                        {
                            print((N2["user"][0]["gender"].Value));
                            if (float.Parse(N2["user"][0]["gender"].Value) == 2)
                            { ScriptSingleton.female = true; }
                            if (float.Parse(N2["user"][0]["gender"].Value) == 1)
                            { ScriptSingleton.female = false; }
                        }
                    }


                    if ((N2["ranking_mayor"].Value) != "")
                    {
                        maximoglobal = Mathf.FloorToInt(float.Parse(N2["ranking_mayor"].Value));
                        maxpersonalrecord = Mathf.FloorToInt(float.Parse(N2["puntos_user"].Value));
                        ScriptSingleton.maximouser = maxpersonalrecord;
                        ScriptSingleton.maximoglobal = maximoglobal;
                    }
                    else
                    {
                        maximoglobal = 0;
                        maxpersonalrecord = 0;
                        ScriptSingleton.maximouser = 0;
                        ScriptSingleton.maximoglobal = 0;
                    }
                }
                menugameloginscreen[0].SetActive(true);
                menugameloginscreen[1].SetActive(false);

                ScriptSingleton.iduser = idUser;
                mastermenu(1);// 1 me activa el menu principal
                textobienvenida.GetComponent<Text>().text = "hola " + User;
                ScriptSingleton.playername = User;
            }
        }

        menugameloginscreen[1].transform.GetChild(2).gameObject.SetActive(false);
    }

    public void getranking()
    {
        posrankpintar = 0;
        ranking.Clear();
        StartCoroutine(GetRanking(idGame));
    }

    IEnumerator GetRanking(int id_game)
    {
        string get_url = URLServer + "/peticiones_game/GetRanking" + '/' + token + '/' + idUser + '/' + id_game; ;
        WWW hs_get = new WWW(get_url);

        //      print (get_url);
        yield return hs_get;
        waiting.SetActive(false);
        userelements.SetActive(true);
        if (hs_get.error != null)
        {
        }
        else
        {
            var N = JSONNode.Parse(hs_get.text);
            ranking.Clear();
            foreach (JSONNode nC in N.Childs)
            {
                foreach (JSONNode n in nC.Childs)
                {
                    ranking.Add(n);
                }
            }
            foreach (JSONNode a in ranking)
            {
                string At = a["me"];
                posrankpintar += 1;
                if (posrankpintar < 11)
                {
                    if (At == "one")
                    { blackbars[posrankpintar - 1].GetComponent<Image>().color = new Color(1f, 1f, 1f, 0.3f); }
                    else
                    { blackbars[posrankpintar - 1].GetComponent<Image>().color = new Color(0f, 0f, 0f, 0.3f); }

                    namesranking[posrankpintar - 1].GetComponent<Text>().text = a["nickname"];
                    puntajeranking[posrankpintar - 1].GetComponent<Text>().text = a["points"];
                }
            }
        }
    }
}
