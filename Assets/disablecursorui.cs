﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disablecursorui : MonoBehaviour
{
    void Update()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        KinematicCharacterController.Examples.ExamplePlayer.lockedcursor = false;
    }
}
