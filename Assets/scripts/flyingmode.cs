﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flyingmode : MonoBehaviour
{

    public GameObject player;
    public Camera godviewcamera;
    public GameObject thirdperson;
    public float sensitivity;
    public float speed=1;
    Vector3 desiredpos;

    public void gothirdperson()
    {
        thirdperson.SetActive(true);
        player.SetActive(false);
        RenderSettings.fog = true;
    }
    public void gogodvew()
    {
        thirdperson.SetActive(false);
        player.SetActive(true);
        RenderSettings.fog = false;
    }

    public GameObject storeui;

    public void settranform(GameObject positiontarget)
    {
        print("called");
        Vector3 telto = positiontarget.transform.position+new Vector3(0, 1, 0)+positiontarget.transform.forward;
        thirdperson.GetComponent<KinematicCharacterController.KinematicCharacterMotor>().SetPosition(telto);
        
    }

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Alpha1))
        {
            gothirdperson();
        }
        if (Input.GetKey(KeyCode.Alpha2))
        {
            gogodvew();
        }

        if (player.activeSelf)
        {
            if (storeui.activeSelf == false)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }

            player.transform.position += (player.transform.right * Input.GetAxis("Vertical") + player.transform.forward * -Input.GetAxis("Horizontal")) * speed / 8;
            if (player.transform.localPosition.y < 10)
            {
                player.transform.position = Vector3.Lerp(player.transform.position, new Vector3(player.transform.position.x,11,player.transform.position.z),Time.deltaTime*4);
            }

        float rotateHorizontal = Input.GetAxis("Mouse X");
            float rotateVertical = Input.GetAxis("Mouse Y");

            transform.RotateAround(player.transform.position, -Vector3.up, -rotateHorizontal * sensitivity);
            player.transform.RotateAround(player.transform.position,-player.transform.forward, -rotateVertical * sensitivity);

            player.GetComponent<Rigidbody>().velocity = Vector3.zero;
            //player.transform.parent.localRotation = Quaternion.Euler(0, player.transform.parent.localRotation.y+ rotateHorizontal, 0);
            //player.transform.localRotation = Quaternion.Euler(0,0, player.transform.localRotation.z+rotateVertical);


        }
        else
        {
            player.transform.position = new Vector3(thirdperson.transform.position.x,60, thirdperson.transform.position.z);
        }
    }



}
