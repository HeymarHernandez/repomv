﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimientocubo : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public float velocidad;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position += new Vector3(velocidad*Time.deltaTime, 0, 0);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position += new Vector3(-velocidad * Time.deltaTime, 0, 0);
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.position += new Vector3(0, 0, velocidad * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.position += new Vector3(0, 0, -velocidad * Time.deltaTime);
        }
    }
}
