﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScriptSingleton : MonoBehaviour
{

    public static ScriptSingleton instancia;
    public static int Puntaje = 0;
    public static int Puntaje2 = 0;
    public static int level;
    public static int lives = 4;
    public static int power = 0;

    public static int iduser;
    public static int idgame;

    public static float maximoglobal;
    public static float maximouser;
    public static string playername;
    public static string masterurl;
    public static string masterkey;
    public static bool showupbaner;
    public static bool keytutorial;
    public static bool female = true;


    public static bool pruebas;

    GameObject gamecontroller;

    void Awake()
    {
        if (instancia == null)
            instancia = this;
        else if (instancia != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }



    public AudioClip[] soundclip;
    public static int tipodeaudio;
    public static bool playonce = false;

    void Update()
    {
        if (playonce)
        {
            if (soundclip[tipodeaudio])
            {
                PlayClipAt(soundclip[tipodeaudio], Camera.main.transform.position);
            }
            playonce = false;
        }
    }

    public void playsound(int newaudiotype)
    {
        tipodeaudio = newaudiotype;
        playonce = true;
    }

    AudioSource PlayClipAt(AudioClip clip, Vector3 pos)
    {
        GameObject tempgo = new GameObject("tempaudio");
        tempgo.transform.position = pos;
        AudioSource aSource = tempgo.AddComponent<AudioSource>();
        aSource.clip = clip;
        aSource.Play();
        aSource.dopplerLevel = 0;
        aSource.spatialBlend = 0;

        Destroy(tempgo, clip.length);
        return aSource;
    }
}
